
test_mysql: test_mysql.o random_string.o
	cc -o test_mysql -L/usr/local/lib -lmariadb test_mysql.o random_string.o

test_mysql.o: test_mysql.c
	cc -c -I/usr/local/include/mysql -o test_mysql.o test_mysql.c

random_string.o: random_string.c
	cc -c -o random_string.o random_string.c

clean:
	rm test_mysql test_mysql.o


