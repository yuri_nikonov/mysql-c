
#include <stdio.h>
#include <stdlib.h>
#include <mysql.h>
#include <time.h>
#include <string.h>

#include "random_string.h"

#define DB_HOST "localhost"
#define DB_USER "java"
#define DB_PASS "password"
#define DB_NAME "test"
#define DB_TABLE "mytable"
#define STRLEN 127


void finish_with_error(MYSQL *con) {
    fprintf(stderr, "%s\n", mysql_error(con));
    mysql_close(con);
    exit(1);
}


int main()
{
    printf("MySQL client version: %s\n", mysql_get_client_info());
    
    random_string_init();
    char rndstring[STRLEN+1], querybuf[STRLEN*2];
    
    MYSQL *con = mysql_init(NULL);
    if (con == NULL) {
        fprintf(stderr, "%s\n", mysql_error(con));
        exit(1);
    }
    if (mysql_real_connect(con, DB_HOST, DB_USER, DB_PASS, DB_NAME, 0, NULL, 0) == NULL) {
        finish_with_error(con);
    }

    if (mysql_query(con, "drop table if exists " DB_TABLE )) {
        finish_with_error(con);
    }
    if (mysql_query(con, "create table " DB_TABLE "( id int not null auto_increment, name varchar(255), primary key(id)) engine=myisam" )) {
        finish_with_error(con);
    }

    
    MYSQL_STMT *stmt;
    if (!(stmt = mysql_stmt_init(con))) {
        fprintf(stderr, "ERROR:mysql_stmt_init() failed.\n");
        exit(1);
    }
    
    
    
    sprintf(querybuf, "insert into mytable (name) values (?)");
    
    if (mysql_stmt_prepare(stmt, querybuf, strlen(querybuf))) {
        fprintf(stderr, "ERROR:mysql_stmt_prepare() failed. Error:%s\nsql:%s\n", mysql_stmt_error(stmt), querybuf);
        exit(1);
    }
    
    MYSQL_BIND input_bind[1];
    memset(input_bind, 0, sizeof(input_bind));
    unsigned long rndstring_len = STRLEN;
    
    input_bind[0].buffer_type = MYSQL_TYPE_VAR_STRING;
    input_bind[0].buffer = rndstring;
    input_bind[0].buffer_length = STRLEN;
    input_bind[0].length = &rndstring_len;
    input_bind[0].is_null = NULL;
    
    if (mysql_stmt_bind_param(stmt, input_bind)) {
        fprintf(stderr, "ERROR:mysql_stmt_bind_param failed\n");
        exit(1);
    }
    // create table mytable ( id int not null auto_increment, name varchar(255), primary key(id)) engine=myisam


    
    for (int j = 0; j < 70000; j++) {

        random_string(rndstring, STRLEN);
        rndstring[STRLEN] = '\0';

        if (mysql_stmt_execute(stmt)) {
            fprintf(stderr, "mysql_stmt_execute(), failed. Error:%s\n", mysql_stmt_error(stmt));
            exit(1);
        }
        if (!(j % 10000)) {
            printf("%d/70000 passed\n", j);
        }
    
    }
    

    if (mysql_stmt_close(stmt)) {
        fprintf(stderr, "mysql_stmt_execute(), failed. Error:%s\n", mysql_stmt_error(stmt));
        exit(1);
    }

    
/*
    if (mysql_query(con, "select * from " DB_TABLE " order by id desc")) {2
        finish_with_error(con);
    }
    
    MYSQL_RES *result = mysql_store_result(con);
    
    if (result == NULL)
        finish_with_error(con);
    
    int num_fields = mysql_num_fields(result);
    
    MYSQL_ROW row;
    printf("+---+-------+\n| id| name  |\n+---+-------+\n");
    while ((row = mysql_fetch_row(result))) {
        printf("| %s | %s |\n", row[0], row[1]);
    }
    
    mysql_free_result(result);
*/
    
    mysql_close(con);
    return 0;
}
