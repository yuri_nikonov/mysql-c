
#include <stdlib.h>
#include <time.h>

union random4 {
    int unsigned intvalue;
    char unsigned charvalue[4];
};

void random_string_init() {
    srandom(time(NULL));
}

void random_string(char *result, int len) {
    union random4 rnd;

    for (int j = 0; j < len; j++) {
        if (!(j % 4))
            rnd.intvalue = random();
        result[j] = ((rnd.charvalue[j % 4] % 95) + 0x20);
    }
}
